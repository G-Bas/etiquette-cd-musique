#!/bin/bash

cd image/

img_artcover='negative-space-classy-industrial-chrome.jpg'
nom=${img_artcover%%.*}
ext=${img_artcover#*.}

nom_cover='etiquette-cd-musique.jpg'
dossier_etiquette='final'

thumbnail_artcover=$nom'-thumbnail'

declare -i largeur_artcover=1500
declare -i hauteur_artcover=$largeur_artcover
declare -i nb_thumbnail_artcover_largeur=3
declare -i nb_thumbnail_artcover_hauteur=3

declare -i l_thumbnail_artcover=$((largeur_artcover / nb_thumbnail_artcover_largeur))
declare -i h_thumbnail_artcover=$((hauteur_artcover / nb_thumbnail_artcover_hauteur))

geometrie_artcover="$l_thumbnail_artcover"'x'"$h_thumbnail_artcover"

declare -i surface_artcover=$((nb_thumbnail_artcover_largeur*nb_thumbnail_artcover_hauteur))

declare -a couleur_artcover=( 0,61,90,1 0,60,77,20 0,57,57,39 0,53,19,58 47,69,0,58 43,44,0,61 38,16,0,63 42,0,13,60 51,0,35,52 )

rm -rf $dossier_etiquette'/'
mkdir -p $dossier_etiquette

base=$thumbnail_artcover'-base.'$ext

convert $img_artcover -resize 50% -gravity center -crop $largeur_artcover'x'$hauteur_artcover'+0+0' -trim $base
convert $base -crop $geometrie_artcover $thumbnail_artcover'.'$ext
rm $base
for i in 0 1 2 3 4 5 6 7 8; do

    
    mogrify -colorspace cmyk -monochrome -colorize ${couleur_artcover[i]} $thumbnail_artcover'-'$i'.'$ext

done

montage -geometry "$geometrie_artcover" -tile $nb_thumbnail_artcover_largeur'x'$nb_thumbnail_artcover_hauteur '*-thumbnail-*.'$ext $nom_cover
mv $nom_cover $dossier_etiquette'/'

for f in 0 1 2 3 4 5 6 7 8; do

    
    rm $thumbnail_artcover'-'$f'.'$ext

done