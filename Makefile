TEX:= $(wildcard *.tex)
PDF:= pdf

.PHONY: image pdf etiquettepdf clean
setup:
	sudo apt install imagemagick
	tlmgr install cd-cover

image:
	bash image.sh

$(PDF):
	mkdir -p $@

etiquettepdf: pdf
	for fichier in $$(find *.tex) ; do \
		pdflatex -output-directory=$(PDF) -halt-on-error $$fichier ; \
	done

clean:
	rm -fr $(PDF)
	rm -fr /image/final/